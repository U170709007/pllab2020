#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float arb_func(float x){
    return x*log10(x) - 1.2;
}

void rf(float *x, float x1, float x2, float fx0, float fx1, int *itr){
    *x = ((x1*fx0) - (x2*fx1))/fx0-fx1;
    ++*itr;
    printf("Iteration %d:, %.5f ", *itr, *x);
    fprintf("Iteration %d:, %.5f ",*itr, *x );
}


int main()
{
    int itr, maxitr;
    float x0,x1, x_curr, x_next,error;
    FILE *outp;
    outp = fopen("rf.txt", "w");
    printf("Enter the x0, x1, error, and maxitr ");
    scanf("%lf %lf %lf %d", &x0, &x1, &error,&maxitr);

    rf(&x_curr, x0,x1,arb_func(x0), arb_func(x1),&itr);

    do{
        if(arb_func(x0)*arb_func(x_curr)<0){
			x1 = x_curr;
		}

           else{
			x0 = x_curr;
           }

           rf(&x_next,x0,x1,arb_func(x0),arb_func(x1),&itr);

           if(fabs(x_next-x_curr)<error){

            printf("Root is: ");


            return 0;
           }
           else{
            x_next=x_curr;
           }
    }
    while(itr<maxitr);

    fclose(outp);
}
